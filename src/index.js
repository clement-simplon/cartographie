/*******************************************VARIABLES************************************************/
let latitudeChambery = 45.568566;
let longitudeChambery = 5.919781;
let urlApiCarParks = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=parkings-enclos-ou-ouvrage-a-chambery&rows=50&facet=delegataire&facet=secteur&facet=type_park&facet=remarque";
let urlApiBikePath = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&rows=500&facet=typeamenag";
let urlFood = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=base-sirene&rows=300&facet=rpet&facet=depet&facet=libcom&facet=siege&facet=libapet&facet=libtefet&facet=saisonat&facet=libnj&facet=libapen&facet=ess&facet=libtefen&facet=categorie&facet=proden&facet=libtu&facet=liborigine&facet=libtca&facet=libreg_new&facet=nom_dept&facet=section&refine.libcom=CHAMBERY&refine.libapet=Restauration+traditionnelle";
let myMap;
let markersCarPark = [];
let markersFood = [];
let polylines = [];
let layerCarPark = L.layerGroup();
let layerFood = L.layerGroup();

let carParkIcon = L.icon({
    iconUrl: '../assets/icons/Templatic-map-icons/automotive.png',

    iconSize: [33, 44], // size of the icon
    iconAnchor: [15, 50], // point of the icon which will correspond to marker's location
    popupAnchor: [0, -40] // point from which the popup should open relative to the iconAnchor
});
let foodIcon = L.icon({
    iconUrl: '../assets/icons/Templatic-map-icons/breakfast-n-brunch.png',

    iconSize: [22, 33], // size of the icon
    iconAnchor: [15, 50], // point of the icon which will correspond to marker's location
    popupAnchor: [0, -40] // point from which the popup should open relative to the iconAnchor
});


/*******************************************FUNCTIONS************************************************/

//function that initializes and displays the map
function initMap(latitude, longitude, zoom) {
    // Create the object 'myMap' and insert in HTML with the ID "map".
    myMap = L.map('map').setView([latitude, longitude], zoom);
    // We get the tiles with openstreetmap so we tell to 'Leaflet' where we get informations. 
    L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {

        attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
        minZoom: 1,
        maxZoom: 20
    }).addTo(myMap);
}

//function that retrieves the json data of url Api
function loadData(url, callback, coord) {
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(json => {
            callback(json, coord);
        })
}


//function that creates markers and popups
function makeMarkersCarPark(json) {
    let arrayObjects = json.records;
    arrayObjects.forEach(function (object, index) {
        let arrayCoords = object.geometry.coordinates;
        let nameCarPark = object.fields.nom_park;
        let adressCarPark = object.fields.adresse;
        let townCarPark = object.fields.commune;
        let zoneCarPark = object.fields.secteur;
        let publicCarPark = object.fields.public;
        let costCarPark = object.fields.remarque;
        let accessCarPark = object.fields.acces;
        let capacityCarPark = object.fields.capacite;
        markersCarPark[index] = L.marker([arrayCoords[1], arrayCoords[0]], {
            icon: carParkIcon
        }).addTo(layerCarPark);
        markersCarPark[index].bindPopup(nameCarPark);
        markersCarPark[index].on('popupopen', function () {
            let infoElem = document.getElementById("info");
            infoElem.innerHTML = nameCarPark + "<br>" + adressCarPark + "<br>" + townCarPark + "<br>" + zoneCarPark + "<br>" + publicCarPark + "<br>" + costCarPark + "<br>" + accessCarPark + "<br>" + capacityCarPark;
        });
        markersCarPark[index].on('popupclose', removeInfo);
    });
}

//function that creates markers and popups for restaurant and bar
function makeMarkersFood(json) {
    let arrayObjects = json.records;
    arrayObjects.forEach(function (object, index) {
        if (object.geometry != undefined) {
            let arrayCoords = object.geometry.coordinates;
            let nameFood = object.fields.l2_declaree;
            if (nameFood == undefined) {
                nameFood = object.fields.enseigne;
            }
            if (nameFood == undefined) {
                nameFood = object.fields.nomen_long;
            }
            let sortFood = object.fields.libactivnat;
            let adressFood = object.fields.l4_normalisee;
            let postalFood = object.fields.l6_normalisee;
            markersFood[index] = L.marker([arrayCoords[1], arrayCoords[0]], {
                icon: foodIcon
            }).addTo(layerFood);
            markersFood[index].bindPopup(nameFood);
            markersFood[index].on('popupopen', function () {
                let infoElem = document.getElementById("info");
                infoElem.innerHTML = nameFood + "<br>" + sortFood + "<br>" + adressFood + "<br>" + postalFood;
            });
            markersFood[index].on('popupclose', removeInfo);
        }
        
    });
}

// function that make polylines for bike path
function makePolyline(json) {
    let arrayObjects = json.records;
    arrayObjects.forEach(function (object, index) {
        let arrayCoords = object.fields.geo_shape.coordinates;
        arrayCoords.forEach(function (coord) {
            let temp = coord[0];
            coord[0] = coord[1];
            coord[1] = temp;
        });
        polylines[index] = L.polyline(arrayCoords).addTo(myMap);
    })
}

//function that removes the content of "info" id
function removeInfo() {
    let infoElem = document.getElementById("info");
    infoElem.textContent = "";
}

//function that runs after a geolocation and run the callback function locateNearestCarPark
function onLocationFound(e) {
    var radius = e.accuracy / 2;
    loadData(urlApiCarParks, locateNearestCarPark, e.latlng);
    L.marker(e.latlng).addTo(myMap)
        .bindPopup("Vous êtes autour de " + radius + " mètres de ce point").openPopup();

    L.circle(e.latlng, radius).addTo(myMap);
}

//function that zoom on geolocation
function geolocation() {

    myMap.locate({
        setView: true,
        maxZoom: 20
    });
}

// function that locate the nearest car park and return his name
function locateNearestCarPark(data, CoordToCompare) {
    let arrayNameCarPark = [];
    let arrayDistance = [];
    let arrayCoord = [];
    let arrayObjects = data.records;
    arrayObjects.forEach(function (object, index) {
        let coordinates = object.geometry.coordinates;
        arrayCoord[index] = coordinates;
        let nameCarPark = object.fields.nom_park;
        arrayNameCarPark[index] = nameCarPark;
        let latlng = L.latLng(coordinates[1], coordinates[0]);
        arrayDistance[index] = CoordToCompare.distanceTo(latlng);
    });
    let diff = arrayDistance[0];
    let index = 0;

    for (let i = 1; i < arrayDistance.length; i++) {
        if (arrayDistance[i] < diff) {
            diff = arrayDistance[i];
            index = i;
        }
    }
    console.log(arrayCoord[index]);
    displayNearestCarPark(arrayNameCarPark[index], arrayDistance[index]);
    itinerary(CoordToCompare,arrayCoord[index]);
}

// function that display on id "info" the information of Car Park
function displayNearestCarPark(name, distance) {
    let infoElem = document.getElementById("info");
    infoElem.innerHTML = "<b>Le parking le plus proche est " + name + "<br>Distance = " + Math.round(distance) + "m</b>";
}

// function that gives the route from point to another point
function itinerary(startPoint,endPoint) {

    let control = L.Routing.control({
        waypoints: [
            L.latLng(startPoint.lat, startPoint.lng),
            L.latLng(endPoint[1], endPoint[0])
        ],
        language: 'fr',
        geocoder: L.Control.Geocoder.nominatim(),
        routeWhileDragging: true,
        reverseWaypoints: true,
        showAlternatives: true,
        altLineOptions: {
            styles: [{
                    color: 'black',
                    opacity: 0.15,
                    weight: 9
                },
                {
                    color: 'white',
                    opacity: 0.8,
                    weight: 6
                },
                {
                    color: 'blue',
                    opacity: 0.5,
                    weight: 2
                }
            ]
        },
        router: L.Routing.mapbox('pk.eyJ1IjoiY2xlbWVudGZsb3JldCIsImEiOiJjanVqdjloMnIwdjV6M3luN3k3Mjg4Ymc5In0.DdHV4pWg9n3y1ZsMQ0f87w')
    }).addTo(myMap);

    L.Routing.errorControl(control).addTo(myMap);
}

function onCheck() {
    if (checkboxCarPark.checked === true) {
        myMap.addLayer(layerCarPark);
    }
    else {
        myMap.removeLayer(layerCarPark);
    }
    if (checkboxFood.checked === true) {
        myMap.addLayer(layerFood);
    }
    else {
        myMap.removeLayer(layerFood);
    }
}

/*******************************************MAIN*****************************************************/
let buttonGeolocation = document.getElementById("geolocation");
let checkboxCarPark = document.getElementById("carpark");
let checkboxFood = document.getElementById("food");

initMap(latitudeChambery, longitudeChambery, 15);
loadData(urlApiBikePath, makePolyline);
loadData(urlApiCarParks, makeMarkersCarPark);
loadData(urlFood, makeMarkersFood);

L.control.polylineMeasure().addTo(myMap);

myMap.on('locationfound', onLocationFound);
buttonGeolocation.addEventListener("click", geolocation);
checkboxCarPark.addEventListener("click", onCheck);
checkboxFood.addEventListener("click", onCheck);